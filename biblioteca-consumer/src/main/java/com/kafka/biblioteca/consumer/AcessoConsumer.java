package com.kafka.biblioteca.consumer;

import com.kafka.biblioteca.model.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec2-jonathan-roberto-1", groupId = "chablau")
    public void receber(@Payload Acesso acesso) {

        try {

        FileWriter arquivo = new FileWriter("/home/a2/kafka-consumer/biblioteca/acessos.txt", true);

        PrintWriter gravarArquivo = new PrintWriter(arquivo);

        gravarArquivo.append("\n"
                + acesso.getCliente() + ";"
                + acesso.getPorta() + ";"
                + acesso.getAtivo());

        gravarArquivo.flush();
        arquivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Tem acesso novo na área: "
                + "\nNome: " + acesso.getCliente()
                + "\nPorta: " + acesso.getPorta()
                + "\nLiberado: " + acesso.getAtivo());

    }

}
