package com.kafka.biblioteca.controller;

import com.kafka.biblioteca.model.Acesso;
import com.kafka.biblioteca.service.AcessoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoProducer acessoProducer;

    @PostMapping("/{cliente}/{porta}")
    public void temAcesso(@PathVariable String cliente, @PathVariable String porta) {
        Acesso acesso = new Acesso();
        acesso.setCliente(cliente);
        acesso.setPorta(porta);
        acessoProducer.enviarAoKafka(acesso);
    }

}
