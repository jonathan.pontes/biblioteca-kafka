package com.kafka.biblioteca.service;

import com.kafka.biblioteca.model.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AcessoProducer {

    //spec2-jonathan-roberto-1 spec2-jonathan-roberto-2 spec2-jonathan-roberto-3

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public void enviarAoKafka(Acesso acesso) {
        Random random = new Random();
        acesso.setAtivo(random.nextBoolean());

        producer.send("spec2-jonathan-roberto-1", acesso);
        System.out.println("Novo acesso postado na fila: "
                + "\nNome: " + acesso.getCliente()
                + "\nPorta: " + acesso.getPorta()
                + "\nLiberado: " + acesso.getAtivo());
    }
}
